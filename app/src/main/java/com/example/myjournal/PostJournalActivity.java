package com.example.myjournal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myjournal.model.Journal;
import com.example.myjournal.util.JournalAPI;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class PostJournalActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int GALLERY_CODE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int CAMERA_REQUEST_CODE = 3;
    private Button saveButton;
    private ImageView addImages;
    private ImageView addPostImage;
    private ImageView addLocation;
    private ImageView addVideo;
    private ImageView imageView;

    private ProgressBar progressBar;
    private TextView postTitle;
    private TextView postDate;
    private EditText title;
    private EditText description;


    private String currentUserId;
    private String currentUserName;

    private String currentPhotoPath;

    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private FirebaseUser user;

    //Connection to firestore
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private StorageReference storageReference;
    private CollectionReference collectionReference = db.collection("Journal");

    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_journal);
        Objects.requireNonNull(getSupportActionBar()).setElevation(0);
        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.post_progressBar);
        postTitle = findViewById(R.id.postTextView);
        postDate  = findViewById(R.id.postDateTextView);
        title = findViewById(R.id.postTitleTextView);
        description = findViewById(R.id.postDescriptionTextView);
        imageView = findViewById(R.id.imageView);

        saveButton = findViewById(R.id.savePost);
        addImages = findViewById(R.id.addImages);
        addLocation = findViewById(R.id.addLocation);
        addPostImage = findViewById(R.id.PostImage);
        addVideo = findViewById(R.id.AddVideo);


        addPostImage.setOnClickListener(this);
        addVideo.setOnClickListener(this);
        saveButton.setOnClickListener(this);
        addImages.setOnClickListener(this);
        addPostImage.setOnClickListener(this);
        addLocation.setOnClickListener(this);


        progressBar.setVisibility(View.INVISIBLE);
        if(JournalAPI.getInstance() !=null){
            currentUserId = JournalAPI.getInstance().getUserId();
            currentUserName = JournalAPI.getInstance().getUsername();
            postTitle.setText(currentUserName);
            Log.d( "oncreateee", "onCreate: currentUserName" + currentUserName);
        }

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if(user!= null){

                }else {

                }
            }
        };
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.savePost:
                saveJournal();
                break;
            case R.id.addImages:

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
                }

//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                if(takePicture.resolveActivity(getPackageManager()) != null){
//                    File photoFile = null;
//                    try{
//                        photoFile = createImageFile();
//                    }catch (IOException e){
//
//                    }
//                    if(photoFile != null){
//                        Uri imageUri = FileProvider.getUriForFile(this,"com.example.android.fileprovider", photoFile);
//                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
//                        startActivityForResult(takePicture, CAMERA_REQUEST_CODE);
//
//                    }
//                }
                break;
            case R.id.addLocation:
                break;
            case R.id.PostImage:
                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_CODE);
                break;
            case R.id.AddVideo:

        }
    }

    private void saveJournal(){
        final String titleJournal = title.getText().toString().trim();
        final String desc = description.getText().toString().trim();
        progressBar.setVisibility(View.VISIBLE);
        if(!TextUtils.isEmpty(titleJournal) && !TextUtils.isEmpty((desc)) && imageUri != null){

            final StorageReference filepath = storageReference.child("journal_images") // /journal_image/my_image_9876575.jpg
                    .child("my_image_"+ Timestamp.now().getSeconds());
            filepath.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    String imageUrl = uri.toString();
                                    Journal journal = new Journal();
                                    journal.setTitle(titleJournal);
                                    journal.setDescription(desc);
                                    journal.setImageUrl(imageUrl); // !!
                                   // Long tsLong = System.currentTimeMillis()/1000;
                                  //  String ts = tsLong.toString();
                                 //   journal.setTimeAdded(new Timestamp(new Date(System.currentTimeMillis())));
                                    journal.setTimeAdded(new Date(System.currentTimeMillis()));
                                    journal.setUserName(currentUserName);
                                    journal.setUserId(currentUserId);

                                    collectionReference.add(journal).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            startActivity(new Intent(PostJournalActivity.this, JournalListActivity.class));
                                            finish();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            progressBar.setVisibility(View.INVISIBLE);
                                            Toast.makeText(PostJournalActivity.this, "Something went worg. Try again!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            });
                            // journal obj
                            // collection ref and add journal obj


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });

        }else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("imageee", "onActivityResult: "+ data);
        if (requestCode == CAMERA_REQUEST_CODE){
            if( data != null){
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                imageUri = getImageUri(this, bitmap);
                imageView.setImageURI(imageUri);
              //  imageView.setImageBitmap(bitmap);
            }
        }
        if ( requestCode == GALLERY_CODE  ){
            if( data != null){
                imageUri = data.getData(); // image path;
                Log.d("imageee", "onActivityResult: "+ imageUri);
                imageView.setImageURI(imageUri);
            }
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    protected void onStart() {
        super.onStart();
        user = firebaseAuth.getCurrentUser();
        firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(firebaseAuth != null){
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawermenu,menu);
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
                if(user != null && firebaseAuth != null){
                    Toast.makeText(PostJournalActivity.this, "Already on this page", Toast.LENGTH_LONG).show();

                    // finish();
                }
                break;
            case R.id.see_posts:
                if(user != null && firebaseAuth != null){
                    //startActivity(new Intent(JournalListActivity.this, PostJournalActivity.class));
                    startActivity(new Intent(PostJournalActivity.this, JournalListActivity.class));
                    // finish();
                }
                break;

            case R.id.action_signout:
                if(user != null && firebaseAuth != null){
                    firebaseAuth.signOut();
                    startActivity(new Intent(PostJournalActivity.this, MainActivity.class));
                    // finish();
                }
        }
        return super.onOptionsItemSelected(item);
    }
}