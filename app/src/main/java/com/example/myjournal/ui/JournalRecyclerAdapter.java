package com.example.myjournal.ui;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myjournal.R;
import com.example.myjournal.model.Journal;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class JournalRecyclerAdapter extends RecyclerView.Adapter<JournalRecyclerAdapter.ViewHolder> implements Filterable {
    private Context context;
    public static final SimpleDateFormat inputFormat = new  SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss");
    private List<Journal> journalList;
    private List<Journal> journalListAll;


    public JournalRecyclerAdapter(Context context, List<Journal> journalList) {
        this.context = context;
        this.journalList = journalList;
        this.journalListAll = new ArrayList<>(journalList);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.journal_row, parent,false);

        return new ViewHolder(view,context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Journal journal = journalList.get(position);
        String imageUrl;
        holder.title.setText(journal.getTitle());
        holder.thoughts.setText(journal.getDescription());
        // picasso to download and show
        imageUrl = journal.getImageUrl();
        Picasso.get().load(imageUrl).placeholder(R.drawable.bck_10).fit().into(holder.image);
        //noinspection deprecation
       // String timeAgo = (String) DateUtils.getRelativeTimeSpanString(journal.getTimeAdded().getSeconds()* 1000);
     //   Log.d("vpe", "onBindViewHolder: before");


            Date now = new Date();
            long seconds= TimeUnit.MILLISECONDS.toSeconds(now.getTime() - journal.getTimeAdded().getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - journal.getTimeAdded().getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - journal.getTimeAdded().getTime());
            long days=TimeUnit.MILLISECONDS.toDays(now.getTime() - journal.getTimeAdded().getTime());
         //   Log.d("vpe", "onBindViewHolder: seconds " + seconds);
            String result;
//
//          System.out.println(TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime()) + " milliseconds ago");
//          System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime()) + " minutes ago");
//          System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime()) + " hours ago");
//          System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime()) + " days ago");

            if(seconds<60)
            {
                result = seconds+ " seconds ago";
            }
            else if(minutes<60)
            {
                result =minutes+" minutes ago";
            }
            else if(hours<24)
            {
                result =hours+" hours ago";
            }
            else
            {
                result = days+" days ago";
            }
     //       Log.d("vpe", "onBindViewHolder: before result +" + result);
            holder.dateAdded.setText(result);

            holder.name.setText(journal.getUserName());




    }

    @Override
    public int getItemCount() {
        return journalList.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            List<Journal> filteredList = new ArrayList<>();
            if(charSequence.toString().isEmpty()){
                filteredList.addAll(journalListAll);
            }else{
                for (Journal j : journalListAll){
                    if(j.getTitle().toLowerCase().contains(charSequence.toString().toLowerCase()) ||
                       j.getDescription().toLowerCase().contains(charSequence.toString().toLowerCase())
                    ){
                        filteredList.add(j);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            journalList.clear();
            journalList.addAll((Collection<? extends Journal>) filterResults.values);
            notifyDataSetChanged();
        }
    };
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, thoughts, dateAdded, name;
        private ImageButton deleteBtn;
        public ImageView image;
        String userId, username;
        public ImageButton shareBtn;
        public ViewHolder(@NonNull View itemView, Context ctx) {
            super(itemView);
            context = ctx;
            deleteBtn = itemView.findViewById(R.id.journal_row_delete);
            title = itemView.findViewById(R.id.journal_title);
            thoughts = itemView.findViewById(R.id.journal_desc);
            dateAdded = itemView.findViewById(R.id.journal_timestamp);
            image = itemView.findViewById(R.id.journal_image);
            name = itemView.findViewById(R.id.journal_row_username);
            shareBtn = itemView.findViewById(R.id.journal_row_share);

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   String titleShare = "";
                   String thoughtsShare = "";
                   String imageUrlShare = "";

                   for ( int i = 0; i< journalList.size(); i++){
                       if(journalList.get(i).getTitle() == title.getText()){
                           titleShare = journalList.get(i).getTitle();
                           thoughtsShare = journalList.get(i).getDescription();
                           imageUrlShare = journalList.get(i).getImageUrl();
                       }

                   }
                    // make the message to be sent
                    String msgToSend = "My Journal: \n" +
                            "\n Title: " + titleShare + " " +
                            "\n\n Thoughts: " + thoughtsShare + " " +
                            "\n\n Image URL: " + imageUrlShare;


                    Intent shareMsg = new Intent(Intent.ACTION_SEND);
                    shareMsg.setType("text/plain");
                    shareMsg.putExtra(Intent.EXTRA_SUBJECT, "My journal");
                    shareMsg.putExtra(Intent.EXTRA_TEXT, msgToSend);
                    context.startActivity(shareMsg);

                }
            });
        }
    }
}





