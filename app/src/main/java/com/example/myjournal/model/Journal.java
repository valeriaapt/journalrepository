package com.example.myjournal.model;

import com.google.firebase.firestore.ServerTimestamp;

import java.sql.Date;
import java.sql.Timestamp;

public class Journal implements Comparable{
    private String title;
    private  String description;
    private String imageUrl;
    private String userId;

    private java.util.Date timeAdded;
    private  String userName;

    public Journal() {
    }

    public Journal(String title, String description, String imageUrl, String userId, Date timeAdded, String userName) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.userId = userId;
        this.timeAdded = timeAdded;
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public java.util.Date getTimeAdded() {
        return timeAdded;
    }

    public void setTimeAdded(java.util.Date timeAdded) {
        this.timeAdded = timeAdded;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public int compareTo(Object o) {
        java.util.Date date =((Journal) o).getTimeAdded();
        return  date.compareTo(this.getTimeAdded());
    }
}
