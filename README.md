
Membrii echipei : Peterca Valeria Cristina 

Cerinte indeplinite

* Implementat o operatie cu camera 
* Implementat un Recycler View 
* Implementat o metoda de Share 
* Persistenta datelor folosind baze de date ( Firestore) 
* Web services ( Firebase Auth si Storage) 

Confluence https://journaldoc.atlassian.net/l/c/1GaKFKq9
